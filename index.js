let turn = 0;

let boardState = {
    player: bigInt(0),
    opponent: bigInt(0)
}

let grid = document.getElementById('c4-grid');

function createCell(id, on, friendly, boardState) {
    let cell = document.createElement('div');
    cell.classList.add('cell');
    cell.id = id.toString();
    if (on) cell.classList.add(friendly ? 'player' : 'opponent');
    cell.textContent = id;

    cell.onclick = () => {
        let key = turn ? 'opponent' : 'player';
        if(isColumnAvailable(boardState, getColumnTop(id))) {
            boardState[key] = placePiece(boardState, boardState[key], id.over(7));
            drawBoard(boardState);

            if (checkForConnection(boardState[key])) gameOver(key);
            else if (isBoardFull(boardState)) gameOver('draw');

            passTurn();
        }
    }
    cell.onmouseenter = () => {
        highlightTopper(id);
    }
    cell.onmouseleave = () => {
        deHightlightTopper(id);
    }
    return cell;
}

function drawBoard(board) {
    let pBoard = board.player;
    let oBoard = board.opponent;

    grid.innerHTML = '';
    for(let y = 0; y < 6; y++) {
        for(let x = 0; x < 7; x++) {
            let id = bigInt(47 - (y + (x * 7)));
            let indexMask = bigInt(1).shiftLeft(id);
            let cell = createCell(
                id,
                !pBoard.or(oBoard).and(indexMask).equals(0),
                !pBoard.and(indexMask).equals(0),
                boardState
            );
            grid.appendChild(cell);
        }
    
    }
}

function passTurn() {
    turn = (turn + 1) % 2;
}

function getColumnTop(id) {
    return id.over(7);
}

function isBoardFull(boardState) {
    let maxBits = bigIntFromBinStr("111111011111101111110111111011111101111110111111");
    return boardState.player.or(boardState.opponent).equals(maxBits);
}

function isColumnAvailable(boardState, col) {
    let board = boardState.player.or(boardState.opponent);
    return board.and(bigInt(1).shiftLeft(5 + (col * 7))).equals(0)
}

function highlightTopper(id) {
    id = id.over(7)
    let cell = document.getElementById(`t${id.toString()}`);
    cell.style.backgroundColor = turn ? 'red' : 'blue';
}

function deHightlightTopper(id) {
    id = id.over(7)
    let cell = document.getElementById(`t${id.toString()}`);
    cell.style.backgroundColor = 'transparent';
}

function findLowestRowAvailable(board, col) {
    let index = -1;
    let c = bigInt(1).shiftLeft(7 * col);
    for(let i = 0; i < 6; ++i) {
        let crow = c.shiftLeft(i);
        if(crow.and(board).equals(0)) return crow;
    }
    return -1;
}

function checkForConnection(bitboard) {
    let horizontal = bitboard.and(bitboard.shiftLeft(7));
    horizontal = horizontal.and(horizontal.shiftLeft(14))

    let vertical = bitboard.and(bitboard.shiftLeft(1));
    vertical = vertical.and(vertical.shiftLeft(2));

    let backslash = bitboard.and(bitboard.shiftLeft(8));
    backslash = backslash.and(backslash.shiftLeft(16));

    let forwardslash = bitboard.and(bitboard.shiftLeft(6));
    forwardslash = forwardslash.and(forwardslash.shiftLeft(12));

    return !horizontal.or(vertical).or(backslash).or(forwardslash).equals(0);
}

// Functions that care about which player
function placePiece(boardState, player, col) {
    let board = boardState.player.or(boardState.opponent);
    let bottomCell = findLowestRowAvailable(board, col);

    return player.or(bottomCell);
}

function bigIntFromBinStr(n) {
    let arr = n.toString(2).split("");
    return bigInt.fromArray(arr, 2, false);
}

function gameOver(winner) {
    let message = winner == 'draw' ? "Game ends in a draw" : `Game Over ${winner} wins`
    alert(message)
    boardState = {
        player: bigInt(0),
        opponent: bigInt(0)
    }
    drawBoard(boardState);

    for(let i = 0; i < 7; i++) {
        let topper = document.getElementById(`t${i}`)
        topper.style.backgroundColor = 'transparent'
    }
}

drawBoard(boardState);